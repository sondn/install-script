#!/usr/bin/env bash

ROOT=`dirname $(readlink -e $BASH_SOURCE)`

echo "BEGIN INSTALLING"

echo "====> COPYING bashrc"

cp assets/.bashrc ~/
source ~/.bashrc

echo "====> YUMMING IMPORTANT PACKAGES"
which emacs > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y nano emacs-nox wget curl tree man screen
fi

echo "====> COPYING emacs.d"
cp -r assets/.emacs.d ~/

echo "====> INSTALLING ADDITIONAL YUM SOURCES"
yum repolist | grep rpmforge > /dev/null 2>&1
if [ $? -eq 1 ]; then
    wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm && rpm -Uhv rpmforge-release*.rf.x86_64.rpm
    rm -rf rpmforge-release*.rf.x86_64.rpm
fi

echo "====> YUMMING IMPORTANT PACKAGES FROM ADDITIONAL SOURCE"
wcho htop > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y htop
fi

echo "====> YUMMING DEVELOPMENT PACKAGES"
which git > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum groupinstall -y 'Development tools'
fi

echo "====> YUMMING PROGRAMMING LANGUAGES"
which java > /dev/null 2>&1
if [ $? -eq 1 ]; then
    yum install -y java-1.6.0-openjdk ruby rubygems python-devel python-setuptools
    easy_install pip
fi

echo "====> INSTALLING GO"
which go > /dev/null 2>&1
if [ $? -eq 1 ]; then
    if [ ! -f go1.2.2.linux-amd64.tar.gz ]; then 
	wget https://storage.googleapis.com/golang/go1.2.2.linux-amd64.tar.gz
    fi
    if [ ! -d go ]; then
	tar xvzf go1.2.2.linux-amd64.tar.gz
    fi
    mv go /usr/local/
    rm -rf go*
fi
mkdir -p /data/go
echo '#GO' >> ~/.bashrc
echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc
echo 'export GOPATH=/data/go' >> ~/.bashrc
echo 'PATH=$PATH:$GOPATH/bin' >> ~/.bashrc

echo "====> INSTALLING NGINX"
which nginx > /dev/null 2>&1
if [ $? -eq 1 ]; then
    if [ ! -f nginx-1.7.2.tar.gz ]; then
	wget http://nginx.org/download/nginx-1.7.2.tar.gz
	tar xvzf nginx-1.7.2.tar.gz
    fi
    yum install -y pcre-devel openssl-devel libatomic_ops-devel
    groupadd nginx > /dev/null 2>&1
    useradd -g nginx -m -s /bin/bash nginx > /dev/null 2>&1
    cd nginx-1.7.2
    ./configure --prefix=/customlibs/nginx-1.7.2 --with-http_ssl_module --with-libatomic --http-client-body-temp-path=/data/nginx/data/client-body --http-proxy-temp-path=/data/nginx/data/proxy --http-fastcgi-temp-path=/data/nginx/data/fastcgi --http-uwsgi-temp-path=/data/nginx/data/uwsgi --http-scgi-temp-path=/data/nginx/data/scgi --with-ipv6
    make && make install
    cd ../
    rm -rf nginx*
fi
echo '#NGINX' >> ~/.bashrc
echo 'export PATH=$PATH:/customlibs/nginx-1.7.2/sbin' >> ~/.bashrc
mkdir -p /data/nginx/sites/default
mkdir -p /data/nginx/logs
mkdir -p /data/nginx/data/client-body
mkdir -p /data/nginx/data/proxy
mkdir -p /data/nginx/data/fastcgi
mkdir -p /data/nginx/data/uwsgi
mkdir -p /data/nginx/data/scgi
chown -R nginx:nginx /data/nginx
cp assets/nginx.conf /customlibs/nginx-1.7.2/conf
mkdir -p /customlibs/nginx-1.7.2/conf/sites-available
mkdir -p /customlibs/nginx-1.7.2/conf/sites-enabled
cp assets/default.conf /customlibs/nginx-1.7.2/conf/sites-available
cd /customlibs/nginx-1.7.2/conf/sites-enabled
rm -rf default.conf
ln -s ../sites-available/default.conf ./
cd $ROOT
cp assets/nginx /etc/init.d
echo "<h1>Hello, world</h1>" > /data/nginx/sites/default/index.html
chkconfig --add nginx
netstat -nap | grep nginx > /dev/null 2>&1
if [ $? -ne 0 ]; then
    service nginx start
fi

echo "INSTALLING DEPLOY SCRIPT"
cd /customlibs
mkdir -p bin etc include init.d lib lib64 sbin lib/pkgconfig
cd $ROOT
cp assets/deploy assets/undeploy /customlibs/bin

echo "INSTALLING MARIADB"
which mysql > /dev/null 2>&1
if [ $? -eq 1 ]; then
    cp assets/mariadb.repo /etc/yum.repos.d/
    yum install -y MariaDB-server MariaDB-client
fi
mkdir -p /data/maria
cp assets/mysql /etc/init.d
netstat -nap | grep mysql > /dev/null 2>&1
if [ $? -ne 0 ]; then
    chown -R mysql:mysql /data/maria
    mysql_install_db --datadir=/data/maria
    chown -R mysql:mysql /data/maria
    service mysql start
    mysql_secure_installation
fi

echo "INSTALLING PHP"
if [ ! -f /customlibs/bin/php ]; then
    if [ ! -f php.tar.bz2 ]; then
	wget http://jp2.php.net/get/php-7.0.4.tar.bz2/from/this/mirror -O php.tar.bz2
	tar xvjf php.tar.bz2
    fi
    yum install -y libxml2-devel curl-devel libpng-devel libjpeg-devel freetype-devel libmcrypt-devel
    cd php-7.0.4
    ./configure --prefix=/customlibs/php-7.0.4 --enable-fpm --with-fpm-user=nginx --with-fpm-group=nginx --with-openssl --with-zlib --with-curl --enable-exif --with-gd --enable-mbstring --with-mcrypt --with-mysql --with-mysqli --with-pdo-mysql --enable-sockets --enable-zip --with-pear --with-config-file-scan-dir=/customlibs/php-7.0.4/lib/conf.d --with-freetype-dir=/usr
    make && make install
    cd ../
    /customlibs/bin/deploy /customlibs/php-7.0.4
    rm -rf php*
fi
rm -rf /customlibs/php-7.0.4/lib/conf.d
mkdir -p /customlibs/php-7.0.4/etc
cp assets/php-fpm /etc/init.d
cp assets/php-fpm.conf /customlibs/php-7.0.4/etc
cp assets/php.ini /customlibs/php-7.0.4/lib
chkconfig --add php-fpm
netstat -nap | grep php > /dev/null 2>&1
if [ $? -ne 0 ]; then
    service php-fpm start
fi
echo "<?php phpinfo(); ?>" > /data/nginx/sites/default/phpinfo.php

echo "INSTALLING PHPMYADMIN"
if [ ! -d /data/nginx/sites/default/pma ]; then
    wget 'http://sourceforge.net/projects/phpmyadmin/files/phpMyAdmin/4.2.1/phpMyAdmin-4.2.1-english.tar.gz/download#!md5!3cc8b0db300c378893e80210eb036870' -O pma.tar.gz
    tar xvzf pma.tar.gz
    mv phpMyAdmin-4.2.1-english /data/nginx/sites/default/pma
    rm -rf pma.tar.gz
fi

echo "INSTALING REDIS"
which redis-server > /dev/null 2>&1
if [ $? -eq 1 ]; then
    if [ ! -f redis-2.8.9.tar.gz ]; then
	wget 'http://download.redis.io/releases/redis-2.8.9.tar.gz'
	tar xvzf redis-2.8.9.tar.gz
    fi
    cd redis-2.8.9
    make
    make PREFIX=/customlibs/redis-2.8.9 install
    /customlibs/bin/deploy /customlibs/redis-2.8.9
    cd ../
    rm -rf redis*
fi
mkdir -p /data/redis/data
mkdir -p /data/redis/conf
cp assets/redis-base.conf /data/redis/conf
cp assets/redis-sample.conf /data/redis/conf

echo "INSTALING HIREDIS"
if [ ! -f /customlibs/hiredis/lib/libhiredis.so ]; then
    if [ ! -d hiredis ]; then
	git clone https://github.com/redis/hiredis
	cd hiredis
	make
	make PREFIX=/customlibs/hiredis install
	/customlibs/bin/deploy /customlibs/hiredis
	cd ../
	rm -rf hiredis
    fi
fi

echo "INSTALLING RETHINKDB"
which rethinkdb > /dev/null 2>&1
if [ $? -ne 0 ]; then
    wget http://download.rethinkdb.com/centos/6/`uname -m`/rethinkdb.repo -O /etc/yum.repos.d/rethinkdb.repo
    yum install -y rethinkdb mercurial
    pip install rethinkdb
fi
mkdir -p /data/rethink
mkdir -p /data/rethink/logs
chkconfig --add rethinkdb
cp assets/rethink.conf /etc/rethinkdb/instances.d
netstat -nap | grep rethink > /dev/null 2>&1
if [ $? -ne 0 ]; then
    rethinkdb create -d /data/rethink/data
    chown -R rethinkdb:rethinkdb /data/rethink
    service rethinkdb start
fi
cp assets/rethink.nginx.conf /customlibs/nginx-1.7.2/conf/sites-available/

echo "INSTALLING BOOST"
if [ ! -f /customlibs/boost-1.55/lib/libboost_atomic.so ]; then
    if [ ! -f boost-1.55.tar.gz ]; then
	wget 'http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.gz/download' -O boost-1.55.tar.gz
	tar xvzf boost-1.55.tar.gz
    fi
    yum install -y libicu-devel
    cd boost_1_55_0
    ./bootstrap.sh --prefix=/customlibs/boost-1.55    
    ./b2 install
    /customlibs/bin/deploy /customlibs/boost-1.55
    cd ..
    rm -rf boost*
fi

echo "INSTALLING LIBEVENT"
if [ ! -f /customlibs/libevent-2.0.21/lib/libevent.so ]; then
    if [ ! -f libevent-2.0.21-stable.tar.gz ]; then
	curl -O 'https://cloud.github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz'
	tar xvzf libevent-2.0.21-stable.tar.gz
    fi
    cd libevent-2.0.21-stable
    ./configure --prefix=/customlibs/libevent-2.0.21
    make && make install
    /customlibs/bin/deploy /customlibs/libevent-2.0.21
    cd ..
    rm -rf libevent*
fi

echo "INSTALLING MEMCACHED"
which memcached > /dev/null 2>&1
if [ $? -ne 0 ]; then
    if [ ! -f memcached-1.4.20.tar.gz ]; then
	wget 'http://memcached.org/files/memcached-1.4.20.tar.gz'
	tar xvzf memcached-1.4.20.tar.gz
    fi
    cd memcached-1.4.20
    ./configure --prefix=/customlibs/memcached-1.4.20
    make && make install
    /customlibs/bin/deploy /customlibs/memcached-1.4.20
    cd ..
    rm -rf memcache*
fi

echo "INSTALLING LIBMEMCACHED"
if [ ! -f /customlibs/libmemcached-1.0.18/lib/libmemcached.so ]; then
    if [ ! -f libmemcached-1.0.18.tar.gz ]; then
	wget 'https://launchpad.net/libmemcached/1.0/1.0.18/+download/libmemcached-1.0.18.tar.gz'
	tar xvzf libmemcached-1.0.18.tar.gz
    fi
    cd libmemcached-1.0.18
    ./configure --prefix=/customlibs/libmemcached-1.0.18
    make && make install
    /customlibs/bin/deploy /customlibs/libmemcached-1.0.18
    cd ..
    rm -rf libmem*
fi

echo "INSTALLING GEARMAN"
which gearmand > /dev/null 2>&1
if [ $? -ne 0 ]; then
    if [ ! -f gearmand-1.1.12.tar.gz ]; then
	wget 'https://launchpad.net/gearmand/1.2/1.1.12/+download/gearmand-1.1.12.tar.gz'
	tar xvzf gearmand-1.1.12.tar.gz
    fi
    yum install -y gperf libuuid-devel
    cd gearmand-1.1.12
    ./configure --prefix=/customlibs/gearmand-1.1.12 --with-boost=/customlibs/boost-1.55
    make && make install
    /customlibs/bin/deploy /customlibs/gearmand-1.1.12
    cd ..
    rm -rf gearmand*
fi

echo "INSTALLING PHP EXTENSIONS"
mkdir -p /customlibs/php-7.0.4/lib/conf.d
if [ ! -f /customlibs/php-7.0.4/lib/php/extensions/no-debug-non-zts-20121212/redis.so ]; then
    pecl install redis
fi
echo 'extension=redis.so' > /customlibs/php-7.0.4/lib/conf.d/redis.ini
if [ ! -f /customlibs/php-7.0.4/lib/php/extensions/no-debug-non-zts-20121212/memcached.so ]; then
    if [ ! -f memcached-2.2.0.tgz ]; then
	wget 'http://pecl.php.net/get/memcached-2.2.0.tgz'
	tar xvzf memcached-2.2.0.tgz
    fi
    cd memcached-2.2.0
    phpize
    ./configure -disable-memcached-sasl --with-libmemcached-dir=/customlibs/libmemcached-1.0.18
    make && make install
    cd ..
    rm -rf mem* package*
fi
echo 'extension=memcached.so' > /customlibs/php-7.0.4/lib/conf.d/memcached.ini
if [ ! -f /customlibs/php-7.0.4/lib/php/extensions/no-debug-non-zts-20121212/gearman.so ]; then
    if [ ! -f gearman-1.1.2.tgz ]; then
	wget 'http://pecl.php.net/get/gearman-1.1.2.tgz'
	tar xvzf gearman-1.1.2.tgz
    fi
    cd gearman-1.1.2
    phpize
    ./configure --with-gearman=/customlibs/gearmand-1.1.12
    make && make install
    cd ..
    rm -rf gear* package*
fi
echo 'extension=gearman.so' > /customlibs/php-7.0.4/lib/conf.d/gearman.ini
service php-fpm restart

echo "====> RUN source ~/.bashrc PLEASE"
